#!/bin/bash

#if [ $# -gt 0 ];then
#        echo there are $# args : $@
#else
#        echo there are 0 args
#fi
MAKE_THEARD=`cat /proc/cpuinfo| grep "processor"| wc -l`

#source build/envsetup.sh

if [ $1 == "clean" ]
	then
	{
		if [ $2 == "u-boot"  -o $2 == "u-boot/" -o $2 == "uboot" ]
			then
			{
				echo make clean u-boot
				pushd u-boot/
				make distclean
				popd
				pushd linux_update/rockdev/
				rm RK3288UbootLoader_V2.30.10.bin
				popd

		}
		elif [ $2 == "kernel" -o $2 == "kernel/" ]
			then
			{
        			echo make clean kernel
				pushd kernel/
				make clean
				popd
				pushd linux_update/rockdev/Image/
				rm kernel.img resource.img
				popd

		}
		elif [ $2 == "android" -o $2 == "android/" ]
			then
			{
        			echo make clean android
				source  build.sh 
				make clean
				pushd linux_update/rockdev/Image/
				rm misc.img boot.img recovery.img system.img
				popd
			}
		else
			{
        			echo make clean u-boot
				pushd u-boot/
				make distclean
				popd
        			echo make clean kernel
				pushd kernel/
				make clean
				popd
        			echo make clean android
				source  build.sh 
				make clean
				pushd rockdev/Image-rk3288_box/
				rm *.img
				popd
				popd linux_update/rockdev/
				rm RK3288UbootLoader_V2.30.10.bin update.img
				popd
				pushd linux_update/rockdev/Image/
				rm kernel.img resource.img misc.img boot.img recovery.img system.img update.img
				popd
				echo clean Img oK
			}
		fi
	}
elif [ $1 == "u-boot" -o $1 == "u-boot/"  -o $1 == "uboot" ]
	then
	{
		pushd u-boot/
		make rk3288_defconfig
		make -j $MAKE_THEARD
		if [ $? -eq 0 ]; then
                        echo "Build uboot ok!"
                else
                        echo "Build uboot failed!"
                        exit 1
                fi
		popd
		#./mkimage.sh
	}
elif [ $1 == "kernel" -o $1 == "kernel/" ]
	then
	{
		pushd kernel/
		#make rp-rk3288_defconfig
		make rp-rk3288.img -j $MAKE_THEARD
		if [ $? -eq 0 ]; then
                        echo "Build kernel ok!"
                else
                        echo "Build kernel failed!"
                        exit 1
                fi
		popd
		cp kernel/resource.img rockdev/Image-rk3288_box/
		cp kernel/kernel.img rockdev/Image-rk3288_box/
		#./mkimage.sh
	}
elif [ $1 == "android" -o $1 == "android/" ]
	then
	{
		source  build.sh
		make -j $MAKE_THEARD
		if [ $? -eq 0 ]; then
                        echo "Build android ok!"
                else
                        echo "Build android failed!"
                        exit 1
                fi
		./mkimage.sh
	}

elif [ $1 == "ota" ]
	then
	{
		source  build.sh 
		. ./build/envsetup.sh
		lunch rk3288_box-userdebug
		./mkimage.sh ota
		make otapackage
	}
else
	{
		pushd u-boot/
		make rk3288_defconfig
		make -j $MAKE_THEARD
		if [ $? -eq 0 ]; then
                        echo "Build uboot ok!"
                else
                        echo "Build uboot failed!"
                        exit 1
                fi
		popd
		
		pushd kernel/ 
		#make rp-rk3288_defconfig
		make rp-rk3288.img -j $MAKE_THEARD
		if [ $? -eq 0 ]; then
                        echo "Build kernel ok!"
                else
                        echo "Build kernel failed!"
                        exit 1
                fi
		popd
		
		source  build.sh 
		#make installclean
		make -j $MAKE_THEARD
		if [ $? -eq 0 ]; then
                        echo "Build android ok!"
                else
                        echo "Build android failed!"
                        exit 1
                fi
		./mkimage.sh 
		. rpupdate.sh
	}
fi
