#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <asm/gpio.h>
#include <linux/gpio.h>
#include <linux/proc_fs.h>
#include <linux/wakelock.h>
#include <linux/err.h>
#include <linux/of_gpio.h>
#include <linux/of_platform.h>
#include <linux/delay.h>

#include <linux/platform_device.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/slab.h>
#include <linux/device.h>
#include <asm/uaccess.h>

struct gpio_gpio {
	int gpio_num;
	int val;
};
int power_5V_reset = 0;

struct power_en_gpio {
	struct gpio_gpio gpio_ctrl1;
	struct gpio_gpio gpio_high1;
	int sleep_flag;
	struct wake_lock rpdzkjctl_wake_lock;
};
static struct power_en_gpio *gpioctrl_data;
static struct proc_dir_entry *gpio_ctl_entry;

static int ytfhgpio_ctl_read(struct file *file, char __user *buf,size_t count, loff_t *ppos)
{
	int len = 0;
        char s[32]={0};

        len = sprintf(s, "GpioLevel=%d\n",1,gpio_get_value(gpioctrl_data->gpio_ctrl1.gpio_num));

        if(copy_to_user(buf,s,len)){
                printk("copy_to_user erro\n");
                return 0;
        }

        return len;
}

static int ytfhgpio_ctl_write(struct file * file,const char __user* buffer,size_t count,loff_t *data)
{
	if( buffer[0] == '0' ){
		gpio_direction_output(gpioctrl_data->gpio_ctrl1.gpio_num, 0);
                gpio_set_value(gpioctrl_data->gpio_ctrl1.gpio_num, 0);
	}else if( buffer[0] == '1' ){
		gpio_direction_output(gpioctrl_data->gpio_ctrl1.gpio_num, 1);
                gpio_set_value(gpioctrl_data->gpio_ctrl1.gpio_num, 1);
	}else{
		gpio_direction_output(gpioctrl_data->gpio_ctrl1.gpio_num, 0);
                gpio_set_value(gpioctrl_data->gpio_ctrl1.gpio_num, 0);
	}
        
        return count;
}
static int ytfhgpio_ctl_open(struct inode *inode, struct file *file)
{
        return 0;
}

static const struct file_operations gpio_ctl_ops = {
        .owner          = THIS_MODULE,
        .open           = ytfhgpio_ctl_open,
        .read           = ytfhgpio_ctl_read,
        .write          = ytfhgpio_ctl_write,
};

static int power_en_probe(struct platform_device *pdev)
{
        int ret = 0;
        struct device_node *np = pdev->dev.of_node;

	gpioctrl_data = devm_kzalloc(&pdev->dev, sizeof(struct power_en_gpio),GFP_KERNEL);
	if (!gpioctrl_data) {
                dev_err(&pdev->dev, "failed to allocate memory\n");
                return -ENOMEM;
        }
	memset(gpioctrl_data, 0, sizeof(struct power_en_gpio));
	
	power_5V_reset = 1;

        gpioctrl_data->gpio_ctrl1.gpio_num = of_get_named_gpio_flags(np, "gpio_ctrl1", 0, NULL);
        if (!gpio_is_valid(gpioctrl_data->gpio_ctrl1.gpio_num))
                gpioctrl_data->gpio_ctrl1.gpio_num = -1;

        gpioctrl_data->gpio_high1.gpio_num = of_get_named_gpio_flags(np, "gpio_high1", 0, NULL);
        if (!gpio_is_valid(gpioctrl_data->gpio_high1.gpio_num))
                gpioctrl_data->gpio_high1.gpio_num = -1;

	gpioctrl_data->sleep_flag = 0;
        of_property_read_u32(np, "rp_is_deep_leep", &gpioctrl_data->sleep_flag);

	platform_set_drvdata(pdev, gpioctrl_data);

	if(gpioctrl_data->gpio_ctrl1.gpio_num != -1){
		ret = gpio_request(gpioctrl_data->gpio_ctrl1.gpio_num, "gpio_ctrl1");
        	if (ret < 0){
			printk("gpioctrl_data->gpio_ctrl1 request error\n");
//      	        return ret;
		}else{
			gpio_direction_output(gpioctrl_data->gpio_ctrl1.gpio_num, 0);
        		gpio_set_value(gpioctrl_data->gpio_ctrl1.gpio_num, 0);
			gpio_ctl_entry = proc_mkdir("ytfh_gpio", NULL);
        		proc_create("gpio_ctrl1",0666,gpio_ctl_entry,&gpio_ctl_ops);
		}
	}

	if(gpioctrl_data->gpio_high1.gpio_num != -1){
		ret = gpio_request(gpioctrl_data->gpio_high1.gpio_num, "gpio_high1");
        	if (ret < 0){
			printk("gpioctrl_data->gpio_high1 request error\n");
//      	        return ret;
		}else{
			gpio_direction_output(gpioctrl_data->gpio_high1.gpio_num, 1);
        		gpio_set_value(gpioctrl_data->gpio_high1.gpio_num, 1);
		}
	}
	
        if(gpioctrl_data->sleep_flag != 0){
                wake_lock_init(&gpioctrl_data->rpdzkjctl_wake_lock,WAKE_LOCK_SUSPEND, "rpdzkj_no_deep_sleep");
                wake_lock(&gpioctrl_data->rpdzkjctl_wake_lock);
        }

        return 0;
}

static int power_en_remove(struct platform_device *pdev)
{
        struct power_en_gpio *gpioctrl_data = platform_get_drvdata(pdev);

	if(gpioctrl_data->gpio_ctrl1.gpio_num != -1){
		gpio_direction_output(gpioctrl_data->gpio_ctrl1.gpio_num, 0);
        	gpio_set_value(gpioctrl_data->gpio_ctrl1.gpio_num, 0);
		gpio_free(gpioctrl_data->gpio_ctrl1.gpio_num);
	}
	if(gpioctrl_data->gpio_high1.gpio_num != -1){
		gpio_direction_output(gpioctrl_data->gpio_high1.gpio_num, 0);
        	gpio_set_value(gpioctrl_data->gpio_high1.gpio_num, 0);
		gpio_free(gpioctrl_data->gpio_high1.gpio_num);
	}
	if(gpioctrl_data->sleep_flag != 0){
                wake_unlock(&gpioctrl_data->rpdzkjctl_wake_lock);
        }


        return 0;
}

static int power_en_suspend(struct platform_device *pdev, pm_message_t state) 
{ 
//	struct platform_device *pdev = to_platform_device(dev);
        struct power_en_gpio *gpioctrl_data = platform_get_drvdata(pdev);
 
        return 0; 
} 
 
static int power_en_resume(struct platform_device *pdev) 
{ 
//	struct platform_device *pdev = to_platform_device(dev);
        struct power_en_gpio *gpioctrl_data = platform_get_drvdata(pdev);
 
        return 0; 
} 


static const struct of_device_id power_en_of_match[] = {
        { .compatible = "ytfh_gpio" },
        { }
};

static struct platform_driver power_en_driver = {
        .probe = power_en_probe,
        .remove = power_en_remove,
        .driver = {
                .name           = "ytfh_gpio_gpio",
                .of_match_table = of_match_ptr(power_en_of_match),
				.owner   = THIS_MODULE,

        },
	.suspend = power_en_suspend,
	.resume = power_en_resume,
};


static int __init rpdzkj_gpio_init(void)
{
	return platform_driver_register(&power_en_driver);
}
subsys_initcall(rpdzkj_gpio_init);

static void __exit rpdzkj_gpio_exit(void)
{
	platform_driver_unregister(&power_en_driver);
}
module_exit(rpdzkj_gpio_exit);


MODULE_LICENSE("GPL");
