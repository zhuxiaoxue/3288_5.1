/*
 * Electromagnetic Pen I2C Driver for Hanvon
 *
 * Copyright (C) 1999-2016  Hanvon Technology Inc.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/i2c.h>
#include <linux/gpio.h>
#include <linux/input.h>
#include <linux/irq.h>
#include <linux/version.h>
#include <linux/slab.h>
#include <linux/device.h>
#include <linux/delay.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/ioctl.h>
#include <asm/uaccess.h>
#include <linux/of_gpio.h>

#define CHIP_NAME					"HW0868"
#define MAX_EVENTS					10
#define SCR_X						1280
#define SCR_Y						800
#define MAX_X						0x27de
#define MAX_Y						0x1cfe
#define MAX_PRESSURE					1024

#define MAX_PACKET_SIZE                			 7

#define DEBUG_SHOW_RAW					0X00000001
#define DEBUG_SHOW_COORD				0X00000010

#define UPDATE_SLAVE_ADDR				0x34

#define HAVE_HANVON_CONFIG_POWER_TOUCH_GPIO	  0 //add by yang for hanvon 0806 TP GPIO config switch

static int hanvon_reset_gpio;
static int hanvon_irq_gpio;
#if HAVE_HANVON_CONFIG_POWER_TOUCH_GPIO
static int hanvon_config_gpio;
static int hanvon_power_gpio;
static int hanvon_touch_gpio;
#endif

#define HW0868_GPIO_RESET				hanvon_reset_gpio
#if HAVE_HANVON_CONFIG_POWER_TOUCH_GPIO
#define HW0868_GPIO_CONFIG				hanvon_config_gpio
#define HW0868_GPIO_POWER				hanvon_power_gpio
#endif

#define HW0868_CMD_RESET				0x08680000
#define HW0868_CMD_CONFIG_HIGH				0x08680001
#define HW0868_CMD_CONFIG_LOW				0x08680002
#define HW0868_CMD_UPDATE				0x08680003
#define HW0868_CMD_GET_VERSION				0x08680004
#define HW0868_CMD_CALIBRATE				0x08680005

/* define pen flags, 7-bytes protocal. */
#define PEN_POINTER_UP					0xa0
#define PEN_POINTER_DOWN				0xa1
#define PEN_BUTTON_UP					0xa2
#define PEN_BUTTON_DOWN					0xa3
#define PEN_RUBBER_UP					0xa4
#define PEN_RUBBER_DOWN					0xa5
#define PEN_ALL_LEAVE					0xe0

struct hanvon_data
{
		u16 		x;
		u16 		y;
		u16 		pressure;
		u8 			flag;
};

struct hanvon_i2c {
	struct workqueue_struct *hw_wq;
	struct work_struct work_irq;
	struct mutex mutex_wq;
	struct i2c_client *client;
	unsigned char work_state;
	struct input_dev *input;
};


/* global I2C client. */
static struct i2c_client *g_client;

/* when pen detected, this flag is set 1 */
static volatile int isPenDetected = 0;

/* DEBUG micro, for user interface. */
static unsigned int debug = 0;

/* version number buffer */
static unsigned char ver_info[9] = {0};
static int ver_size = 9;
bool get_version;
/*
#define hw0868_dbg_raw(fmt, args...)        \
	    if(debug & DEBUG_SHOW_RAW) \
        printk(KERN_INFO "[HW0868 raw]: "fmt, ##args);
#define hw0868_dbg_coord(fmt, args...)    \
	    if(debug & DEBUG_SHOW_COORD) \
        printk(KERN_INFO "[HW0868 coord]: "fmt, ##args);
*/

static void hw0868_reset(void)
{
	printk(KERN_INFO "Hanvon 0868 reset!\n");
	mdelay(50);
	gpio_direction_output(HW0868_GPIO_RESET, 0);
	mdelay(50);
	gpio_direction_output(HW0868_GPIO_RESET, 1);
}

static void hw0868_set_power(int i)
{
	printk(KERN_INFO "Hanvon 0868 set power (%d)\n", i);
#if HAVE_HANVON_CONFIG_POWER_TOUCH_GPIO	
	gpio_direction_output(HW0868_GPIO_POWER, i);
#endif
}

static void hw0868_set_config(int i)
{
	printk(KERN_INFO "Config pin status(%d)\n", i);
#if HAVE_HANVON_CONFIG_POWER_TOUCH_GPIO	
	gpio_direction_output(HW0868_GPIO_CONFIG, i);
#endif
}

static int hw0868_get_version(struct i2c_client *client)
{
	int ret = -1;

	unsigned char ver_cmd[] = {0xcd, 0x5f};
	//hw0868_reset();
	get_version = true;
	ret = i2c_master_send(client, ver_cmd, 2);
	if (ret < 0)
	{
		printk(KERN_INFO "Get version ERROR!\n");
		return ret;
	}
	return ret;
}

int fw_i2c_master_send(const struct i2c_client *client, const char *buf, int count)
{
	int ret;
	struct i2c_adapter *adap = client->adapter;
	struct i2c_msg msg;
	
	printk(KERN_INFO "====== fw_i2c_master_send ======");
	msg.addr = UPDATE_SLAVE_ADDR;
	msg.flags = client->flags & I2C_M_TEN;
	msg.len = count;
	msg.buf = (char *)buf;
	msg.scl_rate = 200 * 1000;

	ret = i2c_transfer(adap, &msg, 1);
	printk(KERN_INFO "[hanvon 0868 update] fw_i2c_master_send  ret = %d\n", ret);
	/* 
	 * If everything went ok (i.e. 1 msg transmitted), return #bytes
	 * transmitted, else error code.
	 */
	return (ret == 1) ? count : ret;
}

int fw_i2c_master_recv(const struct i2c_client *client, char *buf, int count)
{
	struct i2c_adapter *adap = client->adapter;
	struct i2c_msg msg;
	int ret;

	printk(KERN_INFO "===== fw_i2c_master_recv =====");
	msg.addr = UPDATE_SLAVE_ADDR;
	msg.flags = client->flags & I2C_M_TEN;
	msg.flags |= I2C_M_RD;
	msg.len = count;
	msg.buf = buf;
	msg.scl_rate = 200 * 1000;

	ret = i2c_transfer(adap, &msg, 1);
	printk(KERN_INFO "[hanvon 0868 update] fw_i2c_master_recv  ret = %d\n", ret);
	/*
	 * If everything went ok (i.e. 1 msg received), return #bytes received,
	 * else error code.
	 */
	return (ret == 1) ? count : ret;
}


ssize_t hanvon_i2c_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	int count, i;
	unsigned char csw_packet[13] = {1};
	printk(KERN_INFO "Receive CSW package.\n");
	count = fw_i2c_master_recv(g_client, csw_packet, 13);
	if (count < 0)
	{
		return -1;
	}
	printk(KERN_INFO "[num 01] read %d bytes.\n", count);
	for(i = 0; i < count; i++)
	{
		printk(KERN_INFO "%.2x \n", csw_packet[i]);
	}
	return sprintf(buf, "%s", csw_packet);
}

ssize_t hanvon_i2c_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	int ret = 0;

	int cmd = *((int *)buf);
	printk(KERN_INFO "%x\n", buf[3]);
	printk(KERN_INFO "%x\n", buf[2]);
	printk(KERN_INFO "%x\n", buf[1]);
	printk(KERN_INFO "%x\n", buf[0]);
	printk(KERN_INFO "%s is called, count=%d.\n", __func__, count);
	if ((count == 31) && (buf[1] == 0x57) && (buf[0] == 0x48))
	{
		printk(KERN_INFO "Send CBW package.\n");
		ret = fw_i2c_master_send(g_client, buf, count);
		return ret;
	}

	/* transfer file */
	if (count == 32)
	{
		printk(KERN_INFO "Transfer file.\n");
		ret = fw_i2c_master_send(g_client, buf, count);
		return ret;
	}
	

	if ((cmd & 0x08680000) != 0x08680000)
	{
		printk(KERN_INFO "Invalid command (0x%08x).\n", cmd);
		return -1;
	}

	switch(cmd)
	{
		case HW0868_CMD_RESET:
			printk(KERN_INFO "Command: reset.\n");
			hw0868_reset();
			break;
		case HW0868_CMD_CONFIG_HIGH:
			printk(KERN_INFO "Command: set config pin high.\n");
			hw0868_set_config(1);
			break;
		case HW0868_CMD_CONFIG_LOW:
			printk(KERN_INFO "Command: set config pin low.\n");
			hw0868_set_config(0);
			break;
		case HW0868_CMD_GET_VERSION:
			printk(KERN_INFO "Command: get firmware version.\n");
			hw0868_get_version(g_client);
			break;
	}
	return count;
}

DEVICE_ATTR(hw0868_entry, 0666, hanvon_i2c_show, hanvon_i2c_store);

/* get version */
ssize_t show_version(struct device *dev, struct device_attribute *attr,char *buf)
{

	unsigned char version[9] = {0};
	int count = sizeof(version), i;

	printk(KERN_INFO "[num 01] read %d bytes.\n", count);
	printk(KERN_INFO "Version data. %.2x %.2x %.2x %.2x %.2x %.2x %.2x %.2x %.2x,\n", 
				ver_info[0], ver_info[1], ver_info[2], ver_info[3], ver_info[4], 
				ver_info[5], ver_info[6], ver_info[7], ver_info[8]);
	for(i = 0; i < count; i++)
	{
		version[i] = ver_info[i]+1;
		printk(KERN_INFO "%.2x \n", version[i]);
	}
	return sprintf(buf, "%s", version);

}

ssize_t store_version(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	return count;
}

DEVICE_ATTR(version, 0666, show_version, store_version);

static long hw0868_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	return 0;
}

static struct file_operations hanvon_cdev_fops = {
	.owner= THIS_MODULE,
	.unlocked_ioctl= hw0868_ioctl,
};

static struct miscdevice hanvon_misc_dev = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "hw0868",
	.fops = &hanvon_cdev_fops,
};

static struct input_dev * allocate_hanvon_input(void)
{
	int ret;
	struct input_dev *input=NULL;

	input = input_allocate_device();
	if(input == NULL)
	{
		return NULL;
	}

	input->name = "Hanvon electromagnetic pen";
	input->phys = "I2C";
	input->id.bustype = BUS_I2C;
	
	set_bit(EV_ABS, input->evbit);
	__set_bit(INPUT_PROP_DIRECT, input->propbit);
	__set_bit(EV_ABS, input->evbit);
	__set_bit(EV_KEY, input->evbit);
	__set_bit(BTN_TOUCH, input->keybit);
	__set_bit(BTN_STYLUS, input->keybit);
	__set_bit(BTN_TOOL_PEN, input->keybit);
	__set_bit(BTN_TOOL_RUBBER, input->keybit);

	input_set_abs_params(input, ABS_X, 0, MAX_X, 0, 0);
	input_set_abs_params(input, ABS_Y, 0, MAX_Y, 0, 0);
	input_set_abs_params(input, ABS_PRESSURE, 0, MAX_PRESSURE, 0, 0);

	input_set_events_per_packet(input, MAX_EVENTS);
	ret = input_register_device(input);
	if(ret) 
	{
		printk(KERN_INFO "Unable to register input device.\n");
		input_free_device(input);
		input = NULL;
	}
	
	return input;
}

static struct hanvon_data hanvon_get_packet(struct hanvon_i2c *phic)
{
	struct hanvon_data data = {0};
	struct i2c_client *client = phic->client;

	u8 x_buf[MAX_PACKET_SIZE];
	u8 buf[9];
	int count;

	do {
			if(get_version)
			{
				count = i2c_master_recv(client, buf, 9);
				get_version = false;
			}else
			{
				//mdelay(2);
				count = i2c_master_recv(client, x_buf, MAX_PACKET_SIZE);
			}
	}
	while(count == EAGAIN);
	printk(KERN_INFO "Reading data. %.2x %.2x %.2x %.2x %.2x %.2x %.2x, count=%d\n", 
				x_buf[0], x_buf[1], x_buf[2], x_buf[3], x_buf[4], 
				x_buf[5], x_buf[6], count);

	if (buf[0] == 0x80)
	{
		printk(KERN_INFO "Get version number ok!\n");
		printk(KERN_INFO "Reading Version. %.2x %.2x %.2x %.2x %.2x %.2x %.2x %.2x %.2x, count=%d\n", 
				buf[0], buf[1], buf[2], buf[3], buf[4], 
				buf[5], buf[6], buf[7], buf[8],count);		
		memcpy(ver_info, buf, ver_size);
	}

	data.flag = x_buf[0];
	data.x |= ((x_buf[1]&0x7f) << 9) | (x_buf[2] << 2) | (x_buf[6] >> 5); // x
	data.y |= ((x_buf[3]&0x7f) << 9) | (x_buf[4] << 2) | ((x_buf[6] >> 3)&0x03); // y
	data.pressure |= ((x_buf[6]&0x07) << 7) | (x_buf[5]);  // pressure

	return data;
}

static int hanvon_report_event(struct hanvon_i2c *phic)
{
	struct hanvon_data data = {0};
	data = hanvon_get_packet(phic);

	if (data.flag == 0x80)
	{
		return 0;
	}
//	hw0868_dbg_coord(KERN_INFO "x=%d\ty=%d\tpressure=%d\tflag=%d\n", data.x, data.y, data.pressure, data.flag);
//	printk(KERN_INFO "x=%d\ty=%d\tpressure=%d\tflag=%d\n", data.x, data.y, data.pressure, data.flag);
	switch(data.flag)
	{
		case PEN_BUTTON_DOWN:
		{
			input_report_abs(phic->input, ABS_X, data.x);
			input_report_abs(phic->input, ABS_Y, data.y);
			input_report_abs(phic->input, ABS_PRESSURE, data.pressure);
			input_report_key(phic->input, BTN_TOUCH, 1);
			input_report_key(phic->input, BTN_TOOL_PEN, 1);
			input_report_key(phic->input, BTN_STYLUS, 1);
			break;
		}
		case PEN_BUTTON_UP:
		{
			input_report_abs(phic->input, ABS_X, data.x);
			input_report_abs(phic->input, ABS_Y, data.y);
			input_report_abs(phic->input, ABS_PRESSURE, data.pressure);
			input_report_key(phic->input, BTN_TOUCH, 0);
			input_report_key(phic->input, BTN_TOOL_PEN, 0);
			input_report_key(phic->input, BTN_STYLUS, 0);
			break;
		}
		case PEN_RUBBER_DOWN:
		{   
			input_report_abs(phic->input, ABS_X, data.x);
			input_report_abs(phic->input, ABS_Y, data.y);
			input_report_abs(phic->input, ABS_PRESSURE, data.pressure);
			input_report_key(phic->input, BTN_TOUCH, 1);
			input_report_key(phic->input, BTN_TOOL_RUBBER, 1);
			break;
		}
		case PEN_RUBBER_UP:
		{
			input_report_abs(phic->input, ABS_X, data.x);
			input_report_abs(phic->input, ABS_Y, data.y);
			input_report_abs(phic->input, ABS_PRESSURE, data.pressure);
			input_report_key(phic->input, BTN_TOUCH, 0);
			input_report_key(phic->input, BTN_TOOL_RUBBER, 0);
			break;
		}
		case PEN_POINTER_DOWN:
		{
			input_report_abs(phic->input, ABS_X, data.x);
			input_report_abs(phic->input, ABS_Y, data.y);
			input_report_abs(phic->input, ABS_PRESSURE, data.pressure);
			input_report_key(phic->input, BTN_TOUCH, 1);
			input_report_key(phic->input, BTN_TOOL_PEN, 1);
			break;
		}
		case PEN_POINTER_UP:
		{
			input_report_abs(phic->input, ABS_X, data.x);
			input_report_abs(phic->input, ABS_Y, data.y);
			input_report_abs(phic->input, ABS_PRESSURE, data.pressure);
			input_report_key(phic->input, BTN_TOUCH, 0);
			if (isPenDetected == 0)
				input_report_key(phic->input, BTN_TOOL_PEN, 1);
			isPenDetected = 1;
			break;
		}
		case PEN_ALL_LEAVE:
		{
			input_report_abs(phic->input, ABS_X, data.x);
			input_report_abs(phic->input, ABS_Y, data.y);
			input_report_abs(phic->input, ABS_PRESSURE, data.pressure);
			input_report_key(phic->input, BTN_TOUCH, 0);
			input_report_key(phic->input, BTN_TOOL_PEN, 0);
			input_report_key(phic->input, BTN_STYLUS, 0);
			isPenDetected = 0;
			break;
		}
		default:
			printk(KERN_ERR "Hanvon stylus device[0868,I2C]: Invalid input event.\n");
	}
	input_sync(phic->input);
	return 0;
}

static void hanvon_i2c_wq(struct work_struct *work)
{
	struct hanvon_i2c *phid = container_of(work, struct hanvon_i2c, work_irq);
	struct i2c_client *client = phid->client;

	mutex_lock(&phid->mutex_wq);
	hanvon_report_event(phid);
	schedule();
	mutex_unlock(&phid->mutex_wq);
	enable_irq(client->irq);
}

static irqreturn_t hanvon_i2c_interrupt(int irq, void *dev_id)
{
	struct hanvon_i2c *phid = (struct hanvon_i2c *)dev_id;

	disable_irq_nosync(irq);
	queue_work(phid->hw_wq, &phid->work_irq);
	printk(KERN_INFO "%s:Interrupt handled.\n", __func__);

	return IRQ_HANDLED;
}

static int hanvon_gpio_get(struct hanvon_i2c *pdev)
{
	int ret = -1;	
	struct device_node *np ;
	struct device *dev;

	enum of_gpio_flags flags;

	dev = &pdev->client->dev;
	np = dev->of_node;
	
	hanvon_irq_gpio = of_get_named_gpio_flags(np, "irq_gpio", 0, &flags);
	hanvon_reset_gpio = of_get_named_gpio_flags(np, "reset_gpio",  0, &flags);

#if HAVE_HANVON_CONFIG_POWER_TOUCH_GPIO	
	hanvon_config_gpio = of_get_named_gpio_flags(np,"cfg_gpio", 0, &flags);
	hanvon_power_gpio = of_get_named_gpio_flags(np,"em_power", 0, &flags);
	hanvon_touch_gpio = of_get_named_gpio_flags(np,"touch_led", 0, &flags);
#endif

	ret = gpio_request(hanvon_reset_gpio, NULL);
	 if(ret != 0){
		gpio_free(hanvon_reset_gpio);
		return -EIO;
	}
	
#if HAVE_HANVON_CONFIG_POWER_TOUCH_GPIO
	ret = gpio_request(hanvon_config_gpio, NULL);
	 if(ret != 0){
		gpio_free(hanvon_config_gpio);
		return -EIO;
	}

	ret = gpio_request(hanvon_power_gpio, NULL);
	 if(ret != 0){
		gpio_free(hanvon_power_gpio);
		return -EIO;
	}
	ret = gpio_request(hanvon_touch_gpio, NULL);
	 if(ret != 0){
		gpio_free(hanvon_touch_gpio);
		return -EIO;
	}
#endif
	
	ret = gpio_request(hanvon_irq_gpio, NULL);
	 if(ret != 0){
		gpio_free(hanvon_irq_gpio);
		printk("hanvon gpio_request error\n");	
		return -EIO;
	}
	
#if HAVE_HANVON_CONFIG_POWER_TOUCH_GPIO
	gpio_direction_output(hanvon_config_gpio, 0);
	gpio_direction_output(hanvon_power_gpio, 1);
	gpio_direction_output(hanvon_touch_gpio, 0);
#endif
	gpio_direction_output(hanvon_reset_gpio, 1);

	hw0868_reset();
}

static int hanvon_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	int result = -1;
	struct hanvon_i2c *hidp = NULL;
	g_client = client;

	hidp = (struct hanvon_i2c*)kzalloc(sizeof(struct hanvon_i2c), GFP_KERNEL);
	if(!hidp)
	{
		printk(KERN_INFO "request memory failed.\n");
		result = -ENOMEM;
		goto fail1;
	}
	
	hidp->input = allocate_hanvon_input();
	
	hidp->client = client;
	hidp->hw_wq = create_singlethread_workqueue("hw_wq");
	mutex_init(&hidp->mutex_wq);
	INIT_WORK(&hidp->work_irq, hanvon_i2c_wq);
	i2c_set_clientdata(client, hidp);
	
	hanvon_gpio_get(hidp);

	client->irq = gpio_to_irq(hanvon_irq_gpio);
#if HAVE_HANVON_CONFIG_POWER_TOUCH_GPIO		
	printk("hanvon_irq_gpio =%d  irq=%d\n",hanvon_touch_gpio,client->irq); 
#else
	printk("irq=%d\n",client->irq);
#endif
	result = request_irq(client->irq, hanvon_i2c_interrupt, IRQF_DISABLED | IRQF_TRIGGER_LOW /*IRQF_TRIGGER_FALLING*/, client->name, hidp);
	if(result)
	{
		printk(KERN_INFO " Request irq(%d) failed\n", client->irq);
		goto fail2;
	}

	/*  define a entry for update; register misc device  */
	result = misc_register(&hanvon_misc_dev);
	device_create_file(hanvon_misc_dev.this_device, &dev_attr_hw0868_entry);
	device_create_file(hanvon_misc_dev.this_device, &dev_attr_version);

	printk(KERN_INFO "%s done.\n", __func__);
	printk(KERN_INFO "Name of device: %s.\n", client->dev.kobj.name);

	return 0;
fail2:
	free_irq(client->irq, hidp);
	i2c_set_clientdata(client, NULL);
	cancel_work_sync(&hidp->work_irq);
	destroy_workqueue(hidp->hw_wq);	
	input_unregister_device(hidp->input);
	hidp->input = NULL;
fail1:
	kfree(hidp);
	hidp = NULL;
	return result;
}

static int hanvon_i2c_remove(struct i2c_client * client)
{
	return 0;
}

static int hanvon_i2c_suspend(struct i2c_client *client, pm_message_t mesg)
{

	printk(KERN_INFO "-------hw0868-----SUSPEND-----\n");
	/* reset hw0868 chip */
	hw0868_reset();
	return 0;
}

static int hanvon_i2c_resume(struct i2c_client *client)
{
	printk(KERN_INFO "-------hw0868-----RESUME-----\n");
	/* reset hw0868 chip */
	hw0868_reset();
	return 0;
}

//static SIMPLE_DEV_PM_OPS(hanvon_i2c_pm, hanvon_i2c_suspend, hanvon_i2c_resume);

MODULE_DEVICE_TABLE(i2c, hanvon_i2c_idtable);

static struct i2c_device_id hanvon_i2c_idtable[] = {
	{ "hanvon_0868_i2c", 0 }, 
	{ } 
};

static struct of_device_id hanvon_dt_ids[] = {	
	{ .compatible = "hanvon_0868_i2c" },
	{ }
};


static struct i2c_driver hanvon_i2c_driver = {
	.driver = {
		.name 		= "hanvon_i2c",
		.owner 		= THIS_MODULE,
		.of_match_table = of_match_ptr(hanvon_dt_ids),	
//		.pm		= &hanvon_i2c_pm,
	},
	.id_table	= hanvon_i2c_idtable,
	.probe		= hanvon_i2c_probe,
	.remove		= hanvon_i2c_remove,
	.resume		= hanvon_i2c_resume,
	.suspend	= hanvon_i2c_suspend,
};

static int hanvon_i2c_init(void)
{
	printk(KERN_INFO "hw0868 chip initializing ....\n");
	return i2c_add_driver(&hanvon_i2c_driver);
}

static void hanvon_i2c_exit(void)
{
	printk(KERN_INFO "hw0868 driver exit.\n");
	i2c_del_driver(&hanvon_i2c_driver);
}

module_init(hanvon_i2c_init);
module_exit(hanvon_i2c_exit);

module_param(debug, uint, S_IRUGO | S_IWUSR);

MODULE_AUTHOR("Wang Hui");
MODULE_DESCRIPTION("Hanvon Electromagnetic Pen");
MODULE_LICENSE("GPL");
